package client

import (
	"strings"

	"go.etcd.io/etcd/api/v3/mvccpb"
	clientv3 "go.etcd.io/etcd/client/v3"
)

// list a directory
func (clt *EtcdHRCHYClient) List(key string) ([]*Node, error) {
	key, _, err := clt.ensureKey(key)
	if err != nil {
		return nil, err
	}
	// directory start with /
	dir := key
        if !isRoot(key) {
	        dir = dir + "/"
        }

	txn := clt.client.Txn(clt.ctx)

	txn.If(
		clientv3.Compare(
			clientv3.Version(key),
			"=",
			0,
		),
	).Then(
		clientv3.OpGet(dir, clientv3.WithPrefix()),
	).Else(
		clientv3.OpGet(dir, clientv3.WithPrefix()),
	)

	txnResp, err := txn.Commit()
	if err != nil {
		return nil, err
	}

	if len(txnResp.Responses) > 0 {
		rangeResp := txnResp.Responses[0].GetResponseRange()
		return clt.list(dir, rangeResp.Kvs)
	} else {
		// empty directory
		return []*Node{}, nil
	}
}

// pick key/value under the dir
func (clt *EtcdHRCHYClient) list(dir string, kvs []*mvccpb.KeyValue) ([]*Node, error) {
        nodeMap := make(map[string]*Node)

	for _, kv := range kvs {
		path := strings.TrimPrefix(string(kv.Key), dir)
                if path == "" {
                        continue
                }

                name := path
                if slashIdx := strings.Index(name, "/"); slashIdx != -1 {
                        rest := name[slashIdx:]
                        name = name[:slashIdx]

                        if _, ok := nodeMap[name]; ok {
                                nodeMap[name].IsDir = true
                        } else {
                                oldKey := string(kv.Key)
                                newKey := strings.TrimSuffix(oldKey, rest)

                                kv.Key = []byte(newKey)
                                nodeMap[name] = clt.createNode(kv, true)
                        }

                        continue
                }

                if _, ok := nodeMap[name]; !ok {
                        nodeMap[name] = clt.createNode(kv, false)
                }
	}

        nodes := []*Node{}

        for _, nodeValue := range nodeMap {
                nodes = append(nodes, nodeValue)
        }

	return nodes, nil
}
